(function () {

	var twitterApi  = angular.module('twitterApi', ['ngSanitize' ,'twitterApp.services']) ;
	
	twitterApi.controller('twitterApiCtrl' , [ '$scope', '$q','$http', 'twitterService',
				 function ($scope, $q,  $http , twitterService) {

		$scope.tweets= []; 
		$scope.connectedToTwitter = false  ; 
		
   				twitterService.initialize() ; 
			

   		this.connectToAccount = function () {
   				twitterService.connectTwitter().then(function() {
   					$scope.connectedToTwitter = true  ; 
   				}) ; 
   		}
   		this.fetchTweets = function (max) {
   			console.log($scope.connectedToTwitter) ; 
   			if (twitterService.isReady() && $scope.connectedToTwitter) {

				twitterService.getLatestTweets(max).then(function (data) {
					$scope.tweets = $scope.tweets.concat(data); ; 
					console.log($scope.tweets); 
				})  ;  
            } else {
            	console.log("error !! twitter is not ready ???") ; 
            }
   				
   		};  

	}]) ; 
})() ;